import pandas as pd
import jieba


#去除停用词，返回去除停用词后的文本列表
def clean_stopwords(contents):
    contents_list=[]
    stopwords = {}.fromkeys([line.rstrip() for line in open('ensemble-classification/data/stopwords.txt', encoding="utf-8")]) #读取停用词表
    stopwords_list = set(stopwords)
    for row in contents:      #循环去除停用词
        words_list = jieba.lcut(row)
        words = [w for w in words_list if w not in stopwords_list]
        sentence=' '.join(words)   #去除停用词后组成新的句子
        contents_list.append(sentence)
    return contents_list


# 将清洗后的文本和标签写入.csv文件中
def after_clean2csv(contents, labels): #输入为文本列表和标签列表
    columns = ['contents', 'labels']
    save_file = pd.DataFrame(columns=columns, data=list(zip(contents, labels)))
    save_file.to_csv('ensemble-classification/data/clean_data_test.csv', index=False, encoding="utf-8")
 

 
if __name__ == '__main__':
    train_data = pd.read_csv('ensemble-classification/data/online_shopping_10_cats.csv', sep=',',
                             names=['type', 'label', 'review']).astype(str)
    labels=[]
    for i in range(len(train_data['label'])):
        labels.append(train_data['label'][i])
    contents=clean_stopwords(train_data['review'])
    after_clean2csv(contents,labels)
